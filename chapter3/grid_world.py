import numpy as np

class grid_world:
    # state: location - (x-axi, y-axi)
    # action: move vector - (x move: -1,0,1, y move -1,0,1)
    def __init__(self, grid_number_on_one_side=5):
        self.grid_number_on_one_side = grid_number_on_one_side
        self.gamma = 0.9
        self.reset()

    def reset(self):
        self.estimate_value = np.zeros((self.grid_number_on_one_side, self.grid_number_on_one_side))

    
    def step(self, state, action):
        if state[0]==0 and state[1]==1:
            return np.array([4,1], dtype=np.int32), 10.
        if state[0]==0 and state[1]==3:
            return np.array([2,3], dtype=np.int32), 5.
        new_state = state+action
        # if out of grid
        if (new_state[0]<0) or new_state[0] >= self.grid_number_on_one_side or new_state[1] < 0 or new_state[1] >= self.grid_number_on_one_side:
            return state, -1.
        return new_state, 0.

    def bellman_update(self):
        self.reset()
        finish_flag = False
        iteration_number = 0
        while finish_flag==False:
            new_value = np.zeros((self.grid_number_on_one_side, self.grid_number_on_one_side))
            for x_axi in range(self.grid_number_on_one_side):
                for y_axi in range(self.grid_number_on_one_side):
                    new_state_move_left, reward_move_left = self.step(np.array([x_axi,y_axi], dtype=np.int32), np.array([-1,0], dtype=np.int32))
                    new_state_move_right, reward_move_right = self.step(np.array([x_axi,y_axi], dtype=np.int32), np.array([1,0], dtype=np.int32))
                    new_state_move_up, reward_move_up = self.step(np.array([x_axi,y_axi], dtype=np.int32), np.array([0,-1], dtype=np.int32))
                    new_state_move_down, reward_move_down = self.step(np.array([x_axi,y_axi], dtype=np.int32), np.array([0,1], dtype=np.int32))
                    new_value[x_axi,y_axi] = np.max([reward_move_left+self.gamma*self.estimate_value[new_state_move_left[0],new_state_move_left[1]],reward_move_right+self.gamma*self.estimate_value[new_state_move_right[0],new_state_move_right[1]],reward_move_up+self.gamma*self.estimate_value[new_state_move_up[0],new_state_move_up[1]],reward_move_down+self.gamma*self.estimate_value[new_state_move_down[0],new_state_move_down[1]]])
            loss = (self.estimate_value-new_value)**2
            self.estimate_value = new_value
            finish_flag = (0.0==np.sum(loss))
            iteration_number += 1

    def equiprobable_random_policy_estimate(self):
        self.reset()
        finish_flag = False
        iteration_number = 0
        while finish_flag==False:
            new_value = np.zeros((self.grid_number_on_one_side, self.grid_number_on_one_side))
            for x_axi in range(self.grid_number_on_one_side):
                for y_axi in range(self.grid_number_on_one_side):
                    new_state_move_left, reward_move_left = self.step(np.array([x_axi,y_axi], dtype=np.int32), np.array([-1,0], dtype=np.int32))
                    new_state_move_right, reward_move_right = self.step(np.array([x_axi,y_axi], dtype=np.int32), np.array([1,0], dtype=np.int32))
                    new_state_move_up, reward_move_up = self.step(np.array([x_axi,y_axi], dtype=np.int32), np.array([0,-1], dtype=np.int32))
                    new_state_move_down, reward_move_down = self.step(np.array([x_axi,y_axi], dtype=np.int32), np.array([0,1], dtype=np.int32))
                    new_value[x_axi,y_axi] = 0.25*(
                        reward_move_left+
                        self.gamma*self.estimate_value[new_state_move_left[0],new_state_move_left[1]]+
                        reward_move_right+
                        self.gamma*self.estimate_value[new_state_move_right[0],new_state_move_right[1]]+
                        reward_move_up+
                        self.gamma*self.estimate_value[new_state_move_up[0],new_state_move_up[1]]+
                        reward_move_down+
                        self.gamma*self.estimate_value[new_state_move_down[0],new_state_move_down[1]]
                        )
            loss = (self.estimate_value-new_value)**2
            self.estimate_value = new_value
            finish_flag = (0.0==np.sum(loss))
            iteration_number += 1

if __name__ == "__main__":
    grid_world = grid_world()
    grid_world.bellman_update()
    # grid_world.equiprobable_random_policy_estimate()
    print(grid_world.estimate_value)
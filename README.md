# implement_reinforcement_learning_introduction

demo of book: reinforcement learning introduction

- depend on package and kit version:
    - python 3.6.9 
    - numpy 1.16.5
    - seaborn 0.9.0
    - pandas 0.25.1
    - matploblib 3.1.1
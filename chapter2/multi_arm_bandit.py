import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import seaborn as sns
import os

class Bandit():
    def __init__(self, arm_number=10, epsilon=0., step_numbers=1000, initial_estimate_value=0., 
    step_size=None, sampling_average=False, ucb_c = None, gradient=False, baseline=False, 
    true_mean_value=0.):
        self.arm_number, self.epsilon, self.step_numbers = arm_number, epsilon, step_numbers
        self.initial_estimate_value = initial_estimate_value
        self.sampling_average = sampling_average
        self.step_size = step_size
        self.ucb_c = ucb_c
        self.gradient = gradient
        self.baseline = baseline
        self.true_mean_value = true_mean_value
    
    def reset(self):
        self.current_step_number = 0.
        self.true_value = np.random.randn(self.arm_number)+self.true_mean_value
        self.best_action = np.argmax(self.true_value)
        self.estimate_value = np.ones(self.arm_number)*self.initial_estimate_value
        self.best_action_count = 0
        self.best_action_number_list = list()
        self.action_selected_numbers = np.zeros(self.arm_number)
        self.average_reward_list, self.reward_list = list(), list()
        self.pi = np.exp(self.estimate_value)/np.sum(np.exp(self.estimate_value))

    def get_action(self):
        if not None == self.ucb_c:
            ucb_estimate = self.estimate_value+self.ucb_c*np.sqrt(np.log(self.current_step_number+1)/(self.action_selected_numbers+1e-10))
            best_estimate_value = np.max(ucb_estimate)
            return np.random.choice(np.where(best_estimate_value==ucb_estimate)[0])
        if True == self.gradient:
            self.pi = np.exp(self.estimate_value)/np.sum(np.exp(self.estimate_value))
            return np.random.choice(range(self.arm_number), p=self.pi)
        if self.epsilon > np.random.rand():
            return np.random.choice(range(self.arm_number))
        else:
            best_estimate_value = np.max(self.estimate_value)
            return np.random.choice(np.where(best_estimate_value==self.estimate_value)[0])

    def step(self, action):
        self.current_step_number += 1
        return np.random.normal(self.true_value[action], 1, 1)

    def run(self):
        self.reset()
        for _ in range(self.step_numbers):
            action = self.get_action()
            if action == self.best_action:
                self.best_action_count += 1
                self.best_action_number_list.append(1)
            if not action == self.best_action:
                self.best_action_number_list.append(0)
            reward = self.step(action)
            self.reward_list.append(reward)
            self.mean_reward = np.mean(self.reward_list)
            self.action_selected_numbers[action] += 1

            if True == self.sampling_average:
                self.estimate_value[action] = (reward+self.estimate_value[action]*(self.action_selected_numbers[action]-1))/self.action_selected_numbers[action]
            baseline = 0.
            if True == self.baseline:
                baseline = self.mean_reward
            if True == self.gradient:
                one_hot = np.zeros(self.arm_number)
                one_hot[action] = 1
                self.estimate_value += self.step_size*(reward-baseline)*(one_hot-self.pi)
            else:
                self.estimate_value[action] = self.step_size*reward + (1-self.step_size)*self.estimate_value[action]
            

def simulater(arm_number=10, epsilon=0., step_number=1000, run_number=2000, epsilons_and_initial=[[0., 0]], 
step_size=0., sampling_average=True, ucb_c = None, gradient=False, baseline=False, true_mean_value=0.):
    epsilons_reward_list, epsilons_best_action_times_list = list(), list()
    for eps_and_ini in epsilons_and_initial:
        reward_list, best_action_times_list = list(), list()
        bandit = Bandit(arm_number=arm_number, epsilon=eps_and_ini[0], step_numbers=step_number, initial_estimate_value=eps_and_ini[1], 
        step_size=step_size, sampling_average=sampling_average, ucb_c = ucb_c, gradient=gradient, baseline=baseline, true_mean_value=4.)
        for _ in range(run_number):
            bandit.reset()
            bandit.run()
            reward_list.append(bandit.reward_list)
            best_action_times_list.append(bandit.best_action_number_list)

        average_reward_list = np.array(reward_list).mean(axis=0)
        best_action_rate = (np.array(best_action_times_list)).mean(axis=0)
        epsilons_reward_list.append(average_reward_list)
        epsilons_best_action_times_list.append(best_action_rate)
    return epsilons_reward_list, epsilons_best_action_times_list

        

def plot_figure_2_1():
    dataset = pd.DataFrame(np.random.randn(200000,10)+np.random.randn(10), columns=[1,2,3,4,5,6,7,8,9,10])
    sns.violinplot(data=dataset.iloc[:,:])
    plt.show()

def plot_figure_2_2():
    epsilons_and_initial = [[0.,0.], [0.1,0.], [0.01,0.]]
    average_reward_list, best_action_rate = simulater(arm_number=10, step_number=1000, run_number=2000, epsilons_and_initial=epsilons_and_initial, 
    step_size=0., sampling_average=True)

    plt.figure(figsize=(10,20))
    plt.subplot(2,1,1)

    for _ in range(len(average_reward_list)):
        plt.plot(average_reward_list[_], label='epsilon = %.02f' % epsilons_and_initial[_][0])
        plt.xlabel('steps')
        plt.ylabel('average reward')
        plt.legend()

    plt.subplot(2,1,2)

    for _ in range(len(average_reward_list)):
        plt.plot(best_action_rate[_], label='epsilon = %.02f' % epsilons_and_initial[_][0])
        plt.xlabel('steps')
        plt.ylabel('optimal action rate')
        plt.legend()

    plt.show()
    plt.savefig('figures/figure_2_2.png')
    plt.close()

def plot_figure_2_3():
    epsilons_and_initial = [[0., 5.],[0.1, 0.]]
    average_reward_list, best_action_rate = simulater(arm_number=10, step_number=1000, run_number=2000, epsilons_and_initial=epsilons_and_initial, 
    step_size=0.1, sampling_average=False)

    for _ in range(len(average_reward_list)):
        plt.plot(best_action_rate[_], label='$Q_{1}$ = %.02f, $\epsilon$ = %.02f' % (epsilons_and_initial[_][1], epsilons_and_initial[_][0]))
        plt.xlabel('steps')
        plt.ylabel('optimal action rate')
    
    plt.legend()

    plt.show()
    plt.savefig('figures/figure_2_3.png')
    plt.close()

def plot_figure_2_4():
    ucb_average_reward_list, ucb_best_action_rate = simulater(arm_number=10, step_number=1000, run_number=2000, epsilons_and_initial=[[0., 0.]], 
    step_size=0.1, sampling_average=True, ucb_c = 2)
    control_group_reward_list, control_group_best_action_rate = simulater(arm_number=10, step_number=1000, run_number=2000, epsilons_and_initial=[[0.1, 0.]], 
    step_size=0.1, sampling_average=True, ucb_c = None)
    plt.plot(ucb_average_reward_list[0], label='UCB $c$ = %.02f' % 2)
    plt.plot(control_group_reward_list[0], label='$\epsilon$-greedy $\epsilon$=%.02f' % 0.1)
    
    plt.legend()

    plt.show()
    plt.savefig('figures/figure_2_4.png')
    plt.close()

def plot_figure_2_5(arm_number=10, step_number=1000, run_number=2000):
    dot4_withbaseline_average_reward_list, dot4_withbaseline__best_action_rate = simulater(arm_number=arm_number, step_number=step_number, run_number=run_number, epsilons_and_initial=[[0.1, 0.]], 
    step_size=0.4, sampling_average=False, gradient=True, baseline=True, true_mean_value=4.)
    dot4_witotuhbaseline_average_reward_list, dot4_witotuhbaseline_best_action_rate = simulater(arm_number=arm_number, step_number=step_number, run_number=run_number, epsilons_and_initial=[[0.1, 0.]], 
    step_size=0.4, sampling_average=False, gradient=True, baseline=False, true_mean_value=4.)
    dot1_withbaseline_average_reward_list, dot1_withbaseline_best_action_rate = simulater(arm_number=arm_number, step_number=step_number, run_number=run_number, epsilons_and_initial=[[0.1, 0.]], 
    step_size=0.1, sampling_average=False, gradient=True, baseline=True, true_mean_value=4.)
    dot1_witotuhbaseline_average_reward_list, dot1_witotuhbaseline_best_action_rate = simulater(arm_number=arm_number, step_number=step_number, run_number=run_number, epsilons_and_initial=[[0.1, 0.]], 
    step_size=0.1, sampling_average=False, gradient=True, baseline=False, true_mean_value=4.)
    
    plt.plot(dot4_withbaseline__best_action_rate[0], label=r'$\alpha$ = %.02f with baseline' % 0.4)
    plt.plot(dot4_witotuhbaseline_best_action_rate[0], label=r'$\alpha$ = %.02f without baseline' % 0.4)
    plt.plot(dot1_withbaseline_best_action_rate[0], label=r'$\alpha$ = %.02f with baseline' % 0.1)
    plt.plot(dot1_witotuhbaseline_best_action_rate[0], label=r'$\alpha$ = %.02f without baseline' % 0.1)

    plt.legend()

    plt.show()

def plot_figure_2_6(arm_number=10, step_number=1000, run_number=2000):
    parameters = [1/128, 1/64, 1/32, 1/16, 1/8, 1/4, 1/2, 1, 2, 4]
    ucb_mean_reward_list = list()
    greedy_random_average_mean_reward_list = list()
    gradient_greedy_mean_reward_list = list()
    greedy_optimistic_stepsize_mean_reward_list = list()
    for para in parameters:
        ucb_reward_list, _ = simulater(arm_number=10, step_number=step_number, run_number=run_number, epsilons_and_initial=[[0., 0.]], 
        sampling_average=True, ucb_c = para)
        ucb_mean_reward_list.append(np.mean(np.array(ucb_reward_list[0])))

        greedy_random_average_reward_list, _ = simulater(arm_number=arm_number, step_number=step_number, run_number=run_number, epsilons_and_initial=[[para, 0.]], 
    step_size=0.1, sampling_average=True, gradient=False, baseline=False, true_mean_value=0.)
        greedy_random_average_mean_reward_list.append(np.mean(np.array(greedy_random_average_reward_list[0])))

        gradient_reward_list, _ = simulater(arm_number=arm_number, step_number=step_number, run_number=run_number, epsilons_and_initial=[[0.1, 0.]], 
    step_size=para, sampling_average=False, gradient=True, baseline=False, true_mean_value=0.)
        gradient_greedy_mean_reward_list.append(np.mean(np.array(gradient_reward_list[0])))

        greedy_optimistic_stepsize_reward_list, _ = simulater(arm_number=arm_number, step_number=step_number, run_number=run_number, epsilons_and_initial=[[0.1, para]], 
    step_size=0.1, sampling_average=False, gradient=False, baseline=True, true_mean_value=0.)
        greedy_optimistic_stepsize_mean_reward_list.append(np.mean(np.array(greedy_optimistic_stepsize_reward_list[0])))

    plt.plot(ucb_mean_reward_list, label=r'UCB')
    plt.plot(greedy_random_average_mean_reward_list, label=r'$greedy')
    plt.plot(gradient_greedy_mean_reward_list, label=r'gradient')
    plt.plot(greedy_optimistic_stepsize_mean_reward_list, label=r'greedy optimistic stepsize mean reward, $\alpha=%.02f$' % 0.1)

    plt.legend()

    plt.show()

if __name__ == "__main__":
    plot_figure_2_6()